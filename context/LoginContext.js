import React, { createContext, useReducer, useEffect, useState } from 'react'
import { loginReducer } from '../reducers/LoginReducer';
import AsyncStorage from '@react-native-community/async-storage';
import { cos } from 'react-native-reanimated';
const getUser = async () => {
    try {
        const userData = await AsyncStorage.getItem('userData')
        return userData ? JSON.parse(userData) : [];
    } catch (e) {
        console.log('Failed to fetch the data from storage');
    }
}
export const LoginContext = createContext();
const LoginContextProvider = (props) => {
    // console.log('contest call hua');
    const [userData, dispatch] = useReducer(loginReducer, []);
    // console.log('ye wala dekh');
    // console.log(userData);
    useEffect(() => {
        async function fetchUser() {
            const userData = await getUser();
            dispatch({ type: 'ADD_DATA', userData });
        }
        fetchUser();
    }, []);
    // storeData();
    useEffect(() => {
        // This check is required to avoid initial writing to asyncStorage
        if (userData) {
            AsyncStorage.setItem('userData', JSON.stringify(userData))
        }
    }, [userData]);
    return (
        <LoginContext.Provider value={{ userData, dispatch }}>
            {props.children}
        </LoginContext.Provider>
    );
}
export default LoginContextProvider