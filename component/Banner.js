import React from 'react'
import { View, Text,TouchableHighlight,Image,StyleSheet,Button } from 'react-native'

const Banner = () => {
    return (
        <>
            <Image style={{
                resizeMode: "cover",
                height: 150,
                width: '100%'
            }}
                source={require('../assets/img/offerbanner.png')}
            />
            <View style={styles.offerBtnDiv}>
                <View style={{ width: '100%', alignItems: 'center' }}>
                    <TouchableHighlight style={{ width: '80%' }}>
                        <Button title="Best Offers EVEERYDAY" color="#f7931d" />
                    </TouchableHighlight>
                </View>
            </View>
        </>
    )
}
const styles = StyleSheet.create({
    offerBtnDiv: {
        marginTop: 10,
    },
});

export default Banner
