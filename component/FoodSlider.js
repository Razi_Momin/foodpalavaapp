import React from 'react'
import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, Button } from 'react-native'
// import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
const FoodSlider = (props) => {
    const { sliderData } = props;
    const navigation = useNavigation();
    const categoryImages = 'http://www.foodpalava.com/assets/img/uploads/category_img/';
    return (
        <View style={{ marginTop: 20 }}>
            <ScrollView horizontal={true}>
                {
                    sliderData.map((val, key) => {
                        let data = {
                            category_id: val.category_id
                        }
                        return (
                            <TouchableOpacity onPress={() => navigation.navigate('SubMenu', data)} key={key} style={{ height: 130, width: 130, marginLeft: 20, borderWidth: 0.5, borderColor: '#dddddd', padding: 5 }}>
                                <View style={{ flex: 2 }}>
                                    <Image style={{
                                        flex: 1,
                                        height: null,
                                        width: '100%',
                                    }}
                                        // source={categoryImages + val.image_name}
                                        source={{ uri: categoryImages + val.image_name }}
                                    />
                                </View>
                                <View style={{ paddingLeft: 10, paddingVertical: 5 }}>
                                    <Text>View More</Text>
                                </View>
                            </TouchableOpacity>
                        )
                    })
                }

            </ScrollView>
        </View>
    )
}

export default FoodSlider

const styles = StyleSheet.create({})
