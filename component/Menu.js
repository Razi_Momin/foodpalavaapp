import React, { useState, useEffect, useContext } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, TouchableHighlight, Dimensions, StatusBar, FlatList, Button } from 'react-native'
// import { Avatar, Badge, Icon, withBadge } from 'react-native-elements'
import { LoginContext } from '../context/LoginContext';
import { webService } from '../webService';
import Ripple from 'react-native-material-ripple';
import MyText from './MyText';
import { LoginCheck } from '../component/LoginCheck';
import AsyncStorage from '@react-native-community/async-storage';
import { useFocusEffect } from '@react-navigation/native';
const menuImgUrl = 'http://www.foodpalava.com/assets/img/uploads/category_img/';
const { _isLogedInCheck } = (LoginCheck);
const windowHeight = Dimensions.get('window').height;

// console.log(oauth());
const Menu = (props) => {
    const { userData, dispatch } = useContext(LoginContext);
    const { navigation } = props;
    const [menuList, setMenuList] = useState([]);
    const check = _isLogedInCheck();
    const storeData = () => {
        dispatch({
            type: 'ADD_DATA',
            userData: [{
                mobile: 9823771077, name: 'razimomin', token: 'jjmmysd@99j'
            }]
        })
    }
    const removeData = async () => {
        dispatch({
            type: 'REMOVE_DATA'
        })
        // try {
        //     await AsyncStorage.clear();
        //     console.log('done');
        //     return true;
        // }
        // catch (exception) {
        //     console.log('error');
        //     return false;
        // }
    }
    const getData123 = () => {
            console.log(userData);
    }
    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <View style={{ paddingHorizontal: 15, flex: 1, flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                    {
                        !check ? <Ripple onPressIn={() => navigation.push('Login')} rippleColor="#f7931d">
                            <TouchableHighlight >
                                <Text style={{ color: '#ffc107', borderWidth: 1, borderColor: '#ffc107', paddingHorizontal: 15, paddingVertical: 10, borderRadius: 5 }}>Login</Text>
                            </TouchableHighlight>
                        </Ripple> : null

                    }

                </View>
            )
        });
    }, [check])
    useEffect(() => {
        let apiEndpoint = 'Menu_ctrl/get_menu';
        let header = webService.getHeaders({})
        //console.log(postArray)
        webService.get(apiEndpoint, header).then((response) => {

            if (response.status === 200) {
                response = response.data;
                // console.log(response.data);
                // console.log(response.data);
                if (response.success) {
                    setMenuList(response.data);
                } else {
                    // Alert.alert('ERROR', response.message)
                }
            } else {
                // Alert.alert('ERROR', 'Net is not working')
            }
        });
    }, [])

    useFocusEffect(
        React.useCallback(() => {
            // isLoggedIn();
            // setCheck(_isLogedInCheck());
        }, [])
    );
    return (
        <View style={{ backgroundColor: '#e9ecef', flex: 1 }}>
            <FlatList contentContainerStyle={{
                flexGrow: 1,
                padding: 15
            }} keyExtractor={(item, index) => String(index)} data={menuList} renderItem={({ item }) => {
                // console.log(index);
                let data = {
                    category_id: item.category_id,
                    mainMenu: menuList
                }
                return (
                    <Ripple onPressIn={() => navigation.push('SubMenu', data)}>
                        <TouchableOpacity style={{ backgroundColor: '#fff', paddingVertical: 10, paddingHorizontal: 10, marginBottom: 10, borderRadius: 10 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <View style={{ marginRight: 10 }}>
                                    <Image style={{ width: 90, height: 55, borderRadius: 5 }} source={{ uri: menuImgUrl + item.image_name }} />
                                </View>
                                <View style={{ flex: 1 }}>
                                    <View style={{ paddingBottom: 10, marginBottom: 10, borderColor: '#333', borderBottomWidth: 0.6 }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <MyText size={12} style={{ fontSize: 12 }}>{item.category_name} ({item.category_count}) </MyText>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                        <MyText> Starts at {check}  {'\u20B9'} {item.category_price}</MyText>
                                        {/* <Button title='View More' color="#f7931d" /> */}
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </Ripple>

                )

            }} />
            {/* <Button onPress={() => storeData()} title="store data" color="red" />
            <Button onPress={() => getData123()} title="get data" color="pink" />
            <Button onPress={() => removeData()} title="remove data" color="blue" /> */}
        </View>
    )
}

export default Menu

const styles = StyleSheet.create({})
