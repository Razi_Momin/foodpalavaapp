import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Image } from 'react-native'

export default function Footer() {
    return (
        <View style={{
            padding: 15, backgroundColor: 'black', width: '100%', position: 'absolute', bottom: 0
        }}>
            <View style={[styles.footerRow, { paddingTop: 0 }]}>
                <Text style={{ color: '#fff', fontWeight: "bold" }}>CONTACT US</Text>
                <Text style={{ color: '#fff', fontWeight: "bold" }}>+919594114111</Text>
            </View>
            <View style={[styles.footerRow, { justifyContent: 'flex-start' }]}>
                <View>
                    <Image style={{ width: 25, height: 25 }} source={require('../assets/img/time-icon.png')} />
                </View>
                <View style={{ marginLeft: 10 }}>
                    <Text style={styles.textWhite}>Hours</Text>
                    <Text style={styles.textWhite}>Monday-Sunday:11am - 11pm</Text>
                </View>
            </View>
            <View style={{ paddingTop: 15 }}>
                <Text style={[styles.textWhite, { fontSize: 12, textAlign: 'center' }]}>
                    Copyright @2018 Food Palava - ALL Right Reserved.
                </Text>
                <Text style={[styles.textWhite, { textAlign: 'center', fontSize: 10 }]}>
                    Powered by bykchariot india pvt ltd
                </Text>
            </View>
        </View >
    )

}
const styles = StyleSheet.create({
    footerRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 10,
        borderWidth: 1,
        borderBottomColor: '#fff'
    },

    textWhite: {
        color: '#fff'
    }
});
