import React from 'react'
import { StyleSheet, Text, View, FlatList } from 'react-native'
import { StatusBar } from 'react-native';
import { Dimensions } from 'react-native';
import Ripple from 'react-native-material-ripple';
import MyText from './MyText';
const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;
// console.log(StatusBar.currentHeight);
import { useNavigation } from '@react-navigation/native';
import { TouchableOpacity } from 'react-native-gesture-handler';
const SubMenuPopup = (props) => {
    const navigation = useNavigation();
    const { mainMenu, setMenuShow } = props;
    return (
        <>
            <Ripple onPressIn={() => setMenuShow(false)} style={styles.popupOpacity}></Ripple>
            <View style={styles.popupMain}>
                <Text style={{ marginBottom: 10, fontFamily: 'Montserrat-Bold' }}>Categories</Text>
                <FlatList showsVerticalScrollIndicator={false} contentContainerStyle={{
                    flexGrow: 1
                }} style={{ maxHeight: 300 }} keyExtractor={(item, index) => String(index)} data={mainMenu} renderItem={({ item }) => {
                    let data = {
                        category_id: item.category_id,
                        mainMenu
                    }
                    return (
                        <TouchableOpacity onPress={() => navigation.push('SubMenu', data)} style={styles.opupList}>
                            <MyText>{item.category_name}</MyText>
                            <MyText>{item.category_count}</MyText>
                        </TouchableOpacity>
                    )
                }} />

            </View>
        </>
    )
}

export default SubMenuPopup

const styles = StyleSheet.create({
    popupMain: { backgroundColor: '#fff', zIndex: 9999999, position: 'absolute', bottom: 0, width: windowWidth, padding: 15 },
    popupOpacity: { position: 'absolute', left: 0, top: -56, width: '100%', height: windowHeight, backgroundColor: '#333', zIndex: 1, opacity: 0.4 },
    opupList: {
        flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderBottomWidth: 1,
        borderBottomColor: '#e7e7e7', paddingVertical: 10
    }
})
