import React from 'react'
import { View, Text, TouchableHighlight, Image } from 'react-native'

const Header = ({ navigation }) => {

    return (
        React.useLayoutEffect(() => {
            navigation.setOptions({
                headerLeft: () => (
                    <View style={{ paddingHorizontal: 15, flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                        <View>
                            <Image style={{
                                resizeMode: "contain",
                                height: 30,
                                width: 88
                            }}
                                source={require('../assets/img/logo.png')}
                            />
                        </View>
                    </View>
                ),
                headerRight: () => (
                    <View style={{ paddingHorizontal: 15, flex: 1, flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                        <TouchableHighlight>
                            <Text style={{ color: '#ffc107', borderWidth: 1, borderColor: '#ffc107', paddingHorizontal: 15, paddingVertical: 10, borderRadius: 5 }}>Login</Text>
                        </TouchableHighlight>
                    </View>
                )
            });
        }, [navigatiommn])
    );

}

export default Header
