import React, { useState, useEffect } from 'react'
import { Image, Pressable, StyleSheet, Text, View } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import Ripple from 'react-native-material-ripple';
import MyText from './MyText';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faTrash, faMinus, faPlus } from '@fortawesome/free-solid-svg-icons'
import { useFocusEffect } from '@react-navigation/native';
import CartFooter from './CartFooter';
const menuImgUrl = 'http://www.foodpalava.com/assets/img/uploads/cuisine_img/';
const SubMenuFlatList = (props) => {
    // console.log(props);
    const [cartEntry, setCartEntry] = useState([]);
    const [cartPrice, setCartPrice] = useState(0);
    const [cartQuantity, setCartQuantity] = useState(0);
    const { menuList, hideCart } = props;
    console.log(menuList);

    const addEntry = (cuisine_id, cuisine_price, image_name, cuisine_type, cuisine_name) => {
        cuisine_price = parseInt(cuisine_price)
        const foundDupicateId = cartEntry.find((item) => {
            return item.cuisine_id === cuisine_id;
        });
        let newArray = [...cartEntry];
        const elementsIndex = cartEntry.findIndex(element => element.cuisine_id == cuisine_id); // find index
        if (foundDupicateId) {
            // clone new array
            newArray[elementsIndex].quantity = newArray[elementsIndex].quantity + 1 // update value by index
            newArray[elementsIndex].cuisine_price = (newArray[elementsIndex].cuisine_price) + cuisine_price // update value by index
            setCartEntry(newArray); // set state again
        } else {
            setCartEntry([...cartEntry, { cuisine_id, quantity: 1, cuisine_price, image_name, cuisine_type, cuisine_name }]);
        }
        // getData();
    }
    const removeEntry = (cuisine_id, cuisine_price) => {
        cuisine_price = parseInt(cuisine_price)
        const foundDupicateId = cartEntry.find((item) => {
            return item.cuisine_id === cuisine_id;
        });
        if (foundDupicateId) {
            const elementsIndex = cartEntry.findIndex(element => element.cuisine_id == cuisine_id); // find index
            let newArray = [...cartEntry]; // clone new array
            if (newArray[elementsIndex].quantity > 1) {
                newArray[elementsIndex].quantity = newArray[elementsIndex].quantity - 1
                setCartEntry(newArray); // set state again
            } else {
                setCartEntry(cartEntry.filter(item => item.cuisine_id !== cuisine_id));
            }
            newArray[elementsIndex].cuisine_price = (newArray[elementsIndex].cuisine_price) - cuisine_price // update value by index
        }
        additionPrice();

    }
    const storeData = async () => {
        try {
            const value = await AsyncStorage.getItem('LocalcartEntry');
            value !== null ? setCartEntry(JSON.parse(value)) : setCartEntry([]);
        } catch (e) {
            console.log('error');
        }
    }
    const updateData = async () => {
        try {
            cartEntry ? await AsyncStorage.setItem('LocalcartEntry', JSON.stringify(cartEntry)) : setCartEntry([]);
        } catch (e) {
            console.log('error aaya');
        }
    }

    useFocusEffect(
        React.useCallback(() => {
            storeData();
            additionPrice();
        }, [])
    );
    useFocusEffect(
        React.useCallback(() => {
            updateData();
            additionPrice();
        }, [cartEntry])
    );
    const additionPrice = () => {
        if (cartEntry.length > 0) {
            const price = cartEntry.reduce((currentTotal, item) => {
                return item.cuisine_price + currentTotal;
            }, 0);
            setCartPrice(price);
            const quantity = cartEntry.reduce((currentTotal, item) => {
                return item.quantity + currentTotal;
            }, 0);
            setCartQuantity(quantity);
        } else {
            setCartQuantity(0);
        }
    }
    // console.log()


    return (
        <>
            {cartEntry.length > 0 && hideCart ? <CartFooter cartPrice={cartPrice} cartQuantity={cartQuantity} /> : null}
            <FlatList contentContainerStyle={{
                flexGrow: 1,
                padding: 15,
            }} keyExtractor={(item, index) => String(index)} data={menuList} renderItem={({ item }) => {
                var icon = parseInt(item.cuisine_type) ? require('../assets/img/non-veg.png') : require('../assets/img/veg-icon.png');
                const { cuisine_id, cuisine_price, image_name, cuisine_type, cuisine_name } = item
                // console.log(cuisine_id);
                // const value = AsyncStorage.getItem('LocalcartEntry')

                // console.log(cartEntry);
                const playerAvailableCheck = cartEntry.find((item) => {
                    return item.cuisine_id === cuisine_id;
                });
                // console.log(playerAvailableCheck);
                // const active = (playerAvailableCheck) ? 'active' : '';
                return (
                    <Pressable style={styles.mainBox}>
                        <View style={styles.rowDesign}>
                            <View style={{ marginRight: 10 }}>
                                <Image style={styles.ImgStye} source={{ uri: menuImgUrl + item.image_name }} />
                            </View>
                            <View style={{ flex: 1 }}>
                                <View style={styles.rowLeft}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <MyText>{item.cuisine_name} </MyText>
                                        {/* <Text style={{ fontFamily: 'ERASBD-Regular' }}>{item.cuisine_name} </Text> */}
                                    </View>
                                </View>
                                <View style={styles.rowRight}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View>
                                            <Image style={styles.categoryImg} source={icon} />
                                        </View>
                                        <View>
                                            <MyText style={{ fontStyle: 'italic' }}> {'\u20B9'} {item.cuisine_price}</MyText>
                                        </View>
                                    </View>
                                    <View>
                                        {
                                            playerAvailableCheck ?
                                                <View style={styles.addRemove}>
                                                    <Pressable style={styles.addRemoveIcon} onPress={() => removeEntry(cuisine_id, cuisine_price)}>
                                                        <FontAwesomeIcon style={{ color: '#878787' }} icon={faMinus} />
                                                    </Pressable>
                                                    <View style={{ flex: 1, alignItems: 'center' }}>
                                                        <MyText color="#00a551">{playerAvailableCheck.quantity}</MyText>
                                                    </View>
                                                    <Pressable style={styles.addRemoveIcon} onPress={() => addEntry(cuisine_id, cuisine_price, image_name, cuisine_type, cuisine_name)}>
                                                        <FontAwesomeIcon style={{ color: '#00a551' }} icon={faPlus} />
                                                    </Pressable>
                                                </View> : <Ripple onPressIn={() => addEntry(cuisine_id, cuisine_price, image_name, cuisine_type, cuisine_name)} rippleColor="#00a551">
                                                    <View style={{ borderColor: '#00a551', borderWidth: 1, paddingHorizontal: 25, paddingVertical: 5, borderRadius: 5, height: 40, justifyContent: 'center' }}>
                                                        <MyText color="#00a551" size={16}>ADD</MyText>
                                                    </View>
                                                </Ripple>
                                        }
                                    </View>
                                </View>
                            </View>
                        </View>
                    </Pressable>
                )
            }} />
        </>
    )
}

export default SubMenuFlatList

const styles = StyleSheet.create({
    mainBox: {
        backgroundColor: '#fff', paddingVertical: 20, paddingHorizontal: 10, borderRadius: 10,
        borderBottomWidth: 0, borderBottomColor: '#e7e7e7', marginBottom: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.00,
        elevation: 1,
    },
    rowDesign: {
        flexDirection: 'row', alignItems: 'center'
    },
    ImgStye: {
        width: 100, height: 70, borderRadius: 5
    },
    rowLeft: { paddingBottom: 10, borderColor: '#333' },
    rowRight: { flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' },
    categoryImg: { width: 20, height: 20, marginRight: 10 },
    addRemove: { flexDirection: 'row', borderColor: '#e0e0e0', borderWidth: 0.6, borderRadius: 5, justifyContent: 'space-around', width: 90, alignItems: 'center', height: 40 },
    addRemoveIcon: { flex: 2, height: '100%', justifyContent: 'center', alignItems: 'center' }
});
