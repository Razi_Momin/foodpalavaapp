import React, { useState, useEffect } from 'react'
import { Dimensions, Image, Pressable, SafeAreaView, StyleSheet, Text, View } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import SubMenuFlatList from './SubMenuFlatList';
import { useFocusEffect } from '@react-navigation/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faMinus, faPlus } from '@fortawesome/free-solid-svg-icons';
import MyText from './MyText';
import CartFlatList from './CartFlatList';
import Ripple from 'react-native-material-ripple';
import { useNavigation } from '@react-navigation/native';
import BillingRow from './BillingRow';
import { floor } from 'react-native-reanimated';
import { ScrollView } from 'react-native-gesture-handler';
const Cart = (props) => {
    const navigation = useNavigation();
    const windowWidth = Dimensions.get('window').width;
    const windowHeight = Dimensions.get('window').height;
    const [cartEntry, setCartEntry] = useState([])
    const [updateCart, setUpdateCart] = useState(0);
    const [cartPrice, setCartPrice] = useState(0);
    const storeData = async () => {
        if (!cartEntry.length) {
            try {
                const value = await AsyncStorage.getItem('LocalcartEntry');
                value !== null ? setCartEntry(JSON.parse(value)) : setCartEntry([]);
            } catch (e) {
                console.log('error');
            }
        }
    }

    const updateData = async () => {
        try {
            const value = await AsyncStorage.getItem('LocalcartEntry')
            if (value !== null) {
                const price = cartEntry.reduce((currentTotal, item) => {
                    return item.cuisine_price + currentTotal;
                }, 0);
                setCartPrice(price);
                cartEntry ? AsyncStorage.setItem('LocalcartEntry', JSON.stringify(cartEntry)) : setCartEntry([]);
            }
        } catch (e) {
            // error reading value
        }
    }
    useFocusEffect(
        React.useCallback(() => {
            updateData();
        }, [cartEntry])

    );
    useFocusEffect(
        React.useCallback(() => {
            storeData();
        }, [])
    );
    const checkout = () => {

    }
    // console.log(cartEntry);
    return (
        <>
            < View style={{ backgroundColor: '#fff', minHeight: windowHeight }}>
                {
                    cartEntry.length > 0 ?
                        <View style={{ height: '100%' }}>
                            <ScrollView style={{ paddingBottom: 150, maxHeight: windowHeight - 150 }}>
                                <View style={{ padding: 15, paddingTop: 0 }}>
                                    <CartFlatList cartEntry={cartEntry} setCartEntry={(value) => setCartEntry(value)} cartPrice={cartPrice} />
                                </View>
                                <BillingDetails cartPrice={cartPrice} />
                            </ScrollView>
                            <Ripple onPressIn={() => checkout()} style={{ alignItems: 'center', padding: 15, backgroundColor: '#7bc242', position: 'absolute', width: '100%', bottom: 80 }}>
                                <Text style={{ fontFamily: 'Montserrat-Bold', color: '#fff' }}>CHECKOUT</Text>
                            </Ripple>
                        </View>
                        : <EmptyCard navigation={navigation} />
                }
            </View>
        </>
    )
}
const EmptyCard = ({ navigation }) => {
    return (
        <View>
            <View style={{ alignItems: 'center', marginVertical: 10 }}>
                <MyText color="gray">Your cart is empty</MyText>
            </View>
            <View style={{ alignItems: 'center', marginBottom: 10 }}>
                <MyText color="gray">Add something from the menu</MyText>
            </View>
            <View style={{ alignItems: 'center' }}>
                <Ripple onPressIn={() => navigation.navigate('Menu')} style={{ paddingVertical: 5, paddingHorizontal: 15, borderWidth: 1, borderColor: '#f7931d', width: 200 }}>
                    <Text style={{ textAlign: 'center', fontFamily: 'Montserrat-Bold', color: '#f7931d' }}>GO TO MENU</Text>
                </Ripple>
            </View>
        </View>
    )
}
const BillingDetails = (props) => {
    const { cartPrice } = props;
    let DiscountPrice = Math.floor((cartPrice * 40) / 100);
    DiscountPrice = (DiscountPrice > 100) ? 100 : DiscountPrice;
    let extractValue = cartPrice - DiscountPrice;
    let cashBack = Math.floor((extractValue) * 10 / 100);
    const deliveryFees = 15;
    const total = eval([extractValue, deliveryFees].join("+"))
    return (
        <>
            <View style={{ paddingHorizontal: 15 }}>
                <BillingRow name='item Total' price={cartPrice} />
                <BillingRow name='Discount' price={DiscountPrice} />
                <BillingRow name='Cashback' price={cashBack} />
                <BillingRow name='Delivery Charge' price={deliveryFees} />
                <View style={styles.billRow}>
                    <Text style={styles.fontLight}>Total</Text>
                    <Text style={styles.fontLight}>{'\u20B9'} {total}</Text>
                </View>
            </View>
        </>
    )
}
export default Cart

const styles = StyleSheet.create({
    fontLight: { fontFamily: 'Montserrat-Bold' },
    billRow: { alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', paddingVertical: 10, borderBottomColor: '#e7e7e7', borderBottomWidth: 1, paddingHorizontal: 15 }
})
