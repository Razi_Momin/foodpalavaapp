import React, { useState, useEffect } from 'react'
import { View, Text, Button, Image, TouchableHighlight, StyleSheet, Alert, Dimensions, LayoutChangeEvent } from 'react-native'
import { useFocusEffect, useIsFocused } from '@react-navigation/native';
import FoodSlider from './FoodSlider';
import { webService } from '../webService';
import Banner from './Banner';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import Footer from './Footer';
function Home({ navigation }) {
    const windowWidth = Dimensions.get('window').width;
    const windowHeight = Dimensions.get('window').height;
    const [sliderData, setSliderData] = useState([]);
    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: () => (
                <View style={{ paddingHorizontal: 15, flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <View>
                        <Image style={{
                            resizeMode: "contain",
                            height: 30,
                            width: 88
                        }}
                            source={require('../assets/img/logo.png')}
                        />
                    </View>
                </View>
            ),
            headerRight: () => (
                <View style={{ paddingHorizontal: 15, flex: 1, flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                    <TouchableHighlight onPress={() => navigation.push('Login')}>
                        <Text style={{ color: '#ffc107', borderWidth: 1, borderColor: '#ffc107', paddingHorizontal: 15, paddingVertical: 10, borderRadius: 5 }}>Login</Text>
                    </TouchableHighlight>
                </View>
            )
        });
    }, [navigation])
    useFocusEffect(
        React.useCallback(() => {
            let apiEndpoint = 'welcome/get_home_slider';
            let header = webService.getHeaders({})
            //console.log(postArray)
            webService.get(apiEndpoint, header).then((response) => {
                // console.log(response);
                if (response.status === 200) {
                    response = response.data;
                    // console.log(response.data);
                    if (response.success) {
                        setSliderData(response.data);
                    } else {
                        // Alert.alert('ERROR', response.message)
                    }
                } else {
                    // Alert.alert('ERROR', 'Net is not working')
                }
            });
        }, [])
    );
    const [dimensions, setDimensions] = useState({ width: 0, height: 0 })
    const [constatntHeight, setConstatntHeight] = useState(0);
    useEffect(() => {
        setConstatntHeight(windowHeight - dimensions.height + 170);
    }, [constatntHeight])
    return (
        <View style={{ backgroundColor: '#fff', flex: 1 }}>
            <ScrollView style={{ maxHeight: windowHeight - 250 }}>
                <Banner />
                <View onLayout={(event) => {
                    const { x, y, width, height } = event.nativeEvent.layout;
                    setDimensions({ width: width, height: height });
                }} style={{ paddingBottom: 25 }}>
                    <FoodSlider sliderData={sliderData} />
                    <TouchableOpacity style={{ alignItems: 'center', marginTop: 20 }}>
                        <View style={{ width: 200 }}>
                            <Button onPress={() => navigation.push('Menu')} title="Menu" color="#f7931d" />
                        </View>
                    </TouchableOpacity>
                </View>

            </ScrollView>
            <Footer />
        </View>
    )
}
const styles = StyleSheet.create({
    // offerBtnDiv: {
    //     marginTop: 10,
    // },
});
export default Home;
