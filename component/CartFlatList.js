import React from 'react'
import { Dimensions, Image, Pressable, SafeAreaView, StyleSheet, Text, View } from 'react-native'
import MyText from './MyText';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faMinus, faPlus } from '@fortawesome/free-solid-svg-icons';
const CartFlatList = (props) => {
    const { cartEntry, setCartEntry, cartPrice } = props;
    const addCuisine = (cuisineId) => {
        const cuisineDetails = cartEntry.find((item) => {
            return item.cuisine_id === cuisineId;
        });
        const { cuisine_price, quantity } = cuisineDetails;
        let cuisineActualprice = cuisine_price / quantity

        // return false;
        const elementsIndex = cartEntry.findIndex(element => element.cuisine_id == cuisineId); // find index
        let newArray = [...cartEntry];
        newArray[elementsIndex].quantity = newArray[elementsIndex].quantity + 1 // update value by index
        newArray[elementsIndex].cuisine_price = (newArray[elementsIndex].cuisine_price) + cuisineActualprice // update value by index
        setCartEntry(newArray);
        // updateData();
    }
    const removeCuisine = (cuisineId) => {
        const cuisineDetails = cartEntry.find((item) => {
            return item.cuisine_id === cuisineId;
        });
        if (cuisineDetails) {
            const { cuisine_price, quantity } = cuisineDetails;
            let cuisineActualprice = cuisine_price / quantity
            const elementsIndex = cartEntry.findIndex(element => element.cuisine_id == cuisineId); // find index
            let newArray = [...cartEntry];
            if (newArray[elementsIndex].quantity > 1) {
                newArray[elementsIndex].quantity = newArray[elementsIndex].quantity - 1 // update value by index
                newArray[elementsIndex].cuisine_price = (newArray[elementsIndex].cuisine_price) - cuisineActualprice // update value by index
                setCartEntry(newArray);
            } else {
                setCartEntry(cartEntry.filter(item => item.cuisine_id !== cuisineId));
                console.log(cartEntry);
            }
        }
    }
    return (
        <>
            {
                cartEntry.map((val, key) => {
                    return (
                        <View key={val.cuisine_id} style={[styles.mainBox, { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }]} >
                            <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                                <Image style={[styles.vegNon, { marginRight: 10 }]} source={require('../assets/img/non-veg.png')} />
                                <MyText>{val.cuisine_name}</MyText>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1, justifyContent: 'flex-end' }}>
                                <View style={styles.addRemove}>
                                    <Pressable onPress={() => removeCuisine(val.cuisine_id)} style={styles.addRemoveIcon}>
                                        <FontAwesomeIcon style={{ color: '#878787' }} icon={faMinus} />
                                    </Pressable>
                                    <View style={{ alignItems: 'center' }}>
                                        <MyText color="#00a551">{val.quantity}</MyText>
                                    </View>
                                    <Pressable onPress={() => addCuisine(val.cuisine_id)} style={styles.addRemoveIcon}>
                                        <FontAwesomeIcon style={{ color: '#00a551' }} icon={faPlus} />
                                    </Pressable>
                                </View>
                                <View style={{ marginLeft: 10 }}>
                                    <MyText style={{ fontStyle: 'italic' }}> {'\u20B9'} {val.cuisine_price}</MyText>
                                </View>
                            </View>
                        </View >
                    )
                })
            }
        </>
    )
}

export default CartFlatList

const styles = StyleSheet.create({
    vegNon: { width: 20, height: 20 },
    addRemove: { flexDirection: 'row', borderColor: '#e0e0e0', borderWidth: 0.6, borderRadius: 5, justifyContent: 'space-around', width: 90, alignItems: 'center', height: 40 },
    addRemoveIcon: { flex: 2, height: '100%', justifyContent: 'center', alignItems: 'center' },
    mainBox: {
        borderBottomColor: '#e7e7e7',
        borderBottomWidth: 1,
        paddingVertical: 15

    }
})
