import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const MyText = (props) => {
    const { size = 14, color = '#333' } = props;
    return (
        <>
            <Text style={{ fontFamily: 'Montserrat-Regular', fontSize: size, color: color }}>{props.children}</Text>
        </>
    )
}

export default MyText

const styles = StyleSheet.create({})
