import React, { createContext, useReducer, useEffect } from 'react'
import { subMenuReducer } from '../component/SubMenuReducer';
export const SubMenuContext = createContext();
import AsyncStorage from '@react-native-community/async-storage';
const SubMenuContextProvider = (props) => {
    console.log(props);
    // return false;
    // const [cartEntry, setCartEntry] = useState([]);
    const [cartEntry, setCartEntry] = useReducer(subMenuReducer, [], () => {
        // console.log(books);
        // const localdata = localStorage.getItem('books');
        // return localdata ? JSON.parse(localdata) : [];
    })
    const storeData = async () => {
        try {
            const value = await AsyncStorage.getItem('LocalcartEntry');
            value !== null ? setCartEntry(JSON.parse(value)) : setCartEntry([]);
        } catch (e) {
            console.log('error');
        }
    }
    const updateData = async () => {
        try {
            cartEntry ? await AsyncStorage.setItem('LocalcartEntry', JSON.stringify(cartEntry)) : setCartEntry([]);
        } catch (e) {
            console.log('error aaya');
        }
    }
    useEffect(() => {
        // console.log(books);
        // localStorage.setItem('books', JSON.stringify(books));
        storeData();
    }, []);
    useEffect(() => {
        updateData();
    }, [cartEntry]);
    const removeEntry = (id) => {
        const foundDupicateId = cartEntry.find((item) => {
            return item.id === id;
        });
        if (foundDupicateId) {
            const elementsIndex = cartEntry.findIndex(element => element.id == id); // find index
            let newArray = [...cartEntry]; // clone new array
            if (newArray[elementsIndex].quantity > 1) {
                newArray[elementsIndex].quantity = newArray[elementsIndex].quantity - 1 // update value by index
                setCartEntry(newArray); // set state again
            } else {
                setCartEntry(cartEntry.filter(item => item.id !== id));
            }
        }
    }

    return (
        <SubMenuContext.Provider value={{ cartEntry, setCartEntry, removeEntry }}>
            {props.children}
        </SubMenuContext.Provider>
    )
}

export default SubMenuContextProvider

