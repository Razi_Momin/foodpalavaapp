import React, { useEffect, useState, useLayoutEffect, useContext } from 'react'
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { webService } from '../webService';
import Ripple from 'react-native-material-ripple';
import { TextInput, Snackbar } from 'react-native-paper';
import { useFocusEffect } from '@react-navigation/native';
import { useNavigation } from '@react-navigation/native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { LoginContext } from '../context/LoginContext';
import { LoginCheck } from '../component/LoginCheck';
var md5 = require('md5');

const Login = (props) => {
    const navigation = useNavigation();
    const { userData, dispatch } = useContext(LoginContext);

    const { _isLogedInCheck } = (LoginCheck);
    console.log(userData);
    const check = _isLogedInCheck();
    useFocusEffect(
        React.useCallback(() => {
            if (check) {
               navigation.navigate('Menu');
            }
        }, [check])
    );
    const [mobile, setMobile] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [errorMessage, setErrorMessage] = React.useState('');
    const [isLoading, setIsLoading] = React.useState(false);
    const [visible, setVisible] = React.useState(false);
    const onDismissSnackBar = () => setVisible(false);
    const loginUser = () => {
        if (mobile === '' || mobile.length !== 10) {
            setErrorMessage('Please enter valid mobile number');
            setVisible(true);
            return false;
        } else if (password === "" || password.length < 8) {
            setErrorMessage('Please enter 8 digit password');
            setVisible(true);
            return false;
        } else {
            callLogin();
        }
    }

    const callLogin = () => {
        setIsLoading(true);
        let apiEndpoint = 'Login_ctrl/signin';
        let header = webService.getHeaders({});
        let postArray = { mobile_no: mobile, password: md5(password) }
        //console.log(postArray)
        webService.post(apiEndpoint, postArray, header).then((response) => {
            // console.log(response);
            setIsLoading(false);
            if (response.status === 200) {
                response = response.data;
                if (response.success) {
                    console.log(response);
                    dispatch({
                        type: 'ADD_DATA',
                        userData: [response]
                    })
                   // getLocalData(response);

                } else {
                    setErrorMessage(response.message);
                    setVisible(true);
                }
            } else {
                // Alert.alert('ERROR', 'Net is not working')
            }
        });
    }

    return (
        <>
            <Snackbar
                visible={visible}
                onDismiss={onDismissSnackBar}
                duration={2000}
                action={{
                    label: 'Close',
                    onPress: () => {
                        // Do something
                    },
                }}>
                {errorMessage}
            </Snackbar>
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingLeft: 20 }}>
                    <View>
            <Text style={{ fontFamily: 'Montserrat-Bold', fontSize: 28 }}>Login {check}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ backgroundColor: '#8B16FF', height: 60, width: 60, borderRadius: 50 }}>
                        </View>
                        <View style={{ backgroundColor: '#8B16FF', height: 90, width: 45, borderTopLeftRadius: 50, borderBottomLeftRadius: 50, marginLeft: 20 }}>
                        </View>
                    </View>
                </View>
                <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', width: '100%', height: '100%', top: 0 }}>
                    <View style={{ padding: 20 }}>
                        <View style={{
                            shadowOffset: {
                                width: 0,
                                height: 1,
                            },
                            shadowOpacity: 0.18,
                            shadowRadius: 1.00,
                            elevation: 1,
                            backgroundColor: '#fff',
                            paddingVertical: 20,
                            paddingHorizontal: 15,
                            borderRadius: 5
                        }}>
                            <TextInput style={{ backgroundColor: '#fff' }}
                                label="Mobile No"
                                value={mobile}
                                keyboardType={'numeric'}
                                onChangeText={text => setMobile(text)}
                            />
                            <TextInput style={{ backgroundColor: '#fff' }}
                                label="Password" secureTextEntry={true}
                                value={password}
                                onChangeText={text => setPassword(text)}
                            />
                        </View>
                    </View>
                    <View style={{ justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', paddingLeft: 20 }}>
                        <View>
                            <Text style={{ fontFamily: 'Montserrat-Medium' }}>Forgot Password</Text>
                        </View>
                        {
                            isLoading ? <View style={{ paddingRight: 15 }}>
                                <ActivityIndicator size="large" color="#0000ff" />
                            </View> : <Ripple onPressIn={() => loginUser()} style={{ height: 40, width: 200, backgroundColor: '#8B16FF', borderTopLeftRadius: 50, borderBottomLeftRadius: 50, justifyContent: 'center' }}>
                                    <Text style={{ textAlign: 'center', color: '#fff', fontSize: 18 }}>LOGIN</Text>
                                </Ripple>
                        }


                    </View>
                </View>
                <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', position: 'absolute', width: '100%', bottom: 25 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ backgroundColor: '#8B16FF', height: 90, width: 45, borderTopRightRadius: 50, borderBottomRightRadius: 50, marginRight: 20 }}>
                        </View>
                        <View style={{ backgroundColor: '#8B16FF', height: 60, width: 60, borderRadius: 50 }}>
                        </View>
                    </View>
                    <View style={{ paddingRight: 20 }}>
                        <Text style={{ fontFamily: 'Montserrat-Bold', fontSize: 28 }}>SIGNUP</Text>
                    </View>
                </View>
            </View>
        </>
    )

}

export default Login

const styles = StyleSheet.create({})
