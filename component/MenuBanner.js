import React from 'react'
import { StyleSheet, Text, View, ImageBackground, Image } from 'react-native'

const MenuBanner = (props) => {
    const { subCuisineThmbnail, menuImgUrl } = props;
    console.log(subCuisineThmbnail);
    return (
        <View style={{ marginBottom: 10, paddingBottom: 40 }}>
            <View style={{ height: 160 }}>
                <ImageBackground style={{
                    resizeMode: "cover",
                    height: '100%',
                    width: '100%',
                }}
                    source={require('../assets/img/offerbanner.png')}
                />
                <View style={{ backgroundColor: '#00000030', height: '100%', width: '100%', position: 'absolute' }}></View>
                <View style={{ position: 'absolute', left: 0, right: 0, top: 115, justifyContent: 'center', alignItems: 'center' }}>
                    <Image style={{ width: 130, height: 85, zIndex: 999 }} source={{ uri: menuImgUrl + subCuisineThmbnail }} />
                </View>
            </View>
        </View >
    )
}

export default MenuBanner

const styles = StyleSheet.create({})
