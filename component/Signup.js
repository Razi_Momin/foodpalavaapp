import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Ripple from 'react-native-material-ripple';
import { TextInput } from 'react-native-paper';
const Signup = () => {
    const [mobile, setMobile] = React.useState('');
    const [password, setPassword] = React.useState('');
    return (
        <>
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingLeft: 20 }}>
                    <View>
                        <Text style={{ fontFamily: 'Montserrat-Bold', fontSize: 28 }}>Sign Up</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ backgroundColor: '#8B16FF', height: 60, width: 60, borderRadius: 50 }}>
                        </View>
                        <View style={{ backgroundColor: '#8B16FF', height: 90, width: 45, borderTopLeftRadius: 50, borderBottomLeftRadius: 50, marginLeft: 20 }}>
                        </View>
                    </View>
                </View>
                <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', width: '100%', height: '100%', top: 0 }}>
                    <View style={{ padding: 20 }}>
                        <View style={{
                            shadowOffset: {
                                width: 0,
                                height: 1,
                            },
                            shadowOpacity: 0.18,
                            shadowRadius: 1.00,
                            elevation: 1,
                            backgroundColor: '#fff',
                            paddingVertical: 20,
                            paddingHorizontal: 15,
                            borderRadius: 5
                        }}>
                            <TextInput style={{ backgroundColor: '#fff' }}
                                label="Mobile No"
                                value={mobile}
                                onChangeText={text => setMobile(text)}
                            />
                            <TextInput style={{ backgroundColor: '#fff' }}
                                label="Password"
                                value={password}
                                onChangeText={text => setPassword(text)}
                            />
                        </View>
                    </View>
                    <View style={{ justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', paddingLeft: 20 }}>
                        <View>
                            {/* <Text style={{ fontFamily: 'Montserrat-Medium' }}>Forgot Password</Text> */}
                        </View>
                        <Ripple style={{ height: 40, width: 200, backgroundColor: '#8B16FF', borderTopLeftRadius: 50, borderBottomLeftRadius: 50, justifyContent: 'center' }}>
                            <Text style={{ textAlign: 'center', color: '#fff', fontSize: 18 }}>SIGNUP</Text>
                        </Ripple>

                    </View>

                </View>
                <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', position: 'absolute', width: '100%', bottom: 25 }}>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ backgroundColor: '#8B16FF', height: 90, width: 45, borderTopRightRadius: 50, borderBottomRightRadius: 50, marginRight: 20 }}>
                        </View>
                        <View style={{ backgroundColor: '#8B16FF', height: 60, width: 60, borderRadius: 50 }}>
                        </View>

                    </View>
                    <View style={{ paddingRight: 20 }}>
                        <Text style={{ fontFamily: 'Montserrat-Bold', fontSize: 28 }}>LOGIN</Text>
                    </View>
                </View>

            </View>

        </>
    )
}

export default Signup

const styles = StyleSheet.create({})
