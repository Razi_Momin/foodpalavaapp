import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const BillingRow = (props) => {
    const { name, price } = props;
    return (
        <View style={styles.billRow}>
            <Text style={styles.fontLight}>{name}</Text>
            <Text style={styles.fontLight}>{'\u20B9'} {price}</Text>
        </View>
    )
}

export default BillingRow

const styles = StyleSheet.create({
    fontLight: { fontFamily: 'Montserrat-Light' },
    billRow: { alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', paddingVertical: 10, borderBottomColor: '#e7e7e7', borderBottomWidth: 1, paddingHorizontal: 15 }
})
