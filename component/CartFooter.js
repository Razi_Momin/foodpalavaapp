import React, { useEffect } from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import MyText from './MyText'
import { useNavigation } from '@react-navigation/native';
import { TouchableOpacity } from 'react-native-gesture-handler';
const CartFooter = (props) => {
    const navigation = useNavigation();
    const { cartPrice,cartQuantity } = props;
    useEffect(() => {
        storeData();
    }, [])
    const storeData = async () => {
        try {
            const value = await AsyncStorage.getItem('LocalcartEntry');
            if (value !== null) {
                const data = JSON.parse(value);
                //console.log("cart me aaya");
                console.log(data);
            }
            // value !== null ? setCartEntry(JSON.parse(value)) : setCartEntry([]);
        } catch (e) {
            console.log('error');
        }
    }
    // const storePriceQuanity = () => {
    //     try {
    //         const priceValue = await AsyncStorage.getItem('totalPrice');
    //         const quantityValue = await AsyncStorage.getItem('totalQuantity');
    //         priceValue !== null ? setCartPrice(cartPrice) : setCartPrice(0);
    //         quantityValue !== null ? setCartQuantity(cartQuantity) : setCartPrice(0);
    //     } catch (e) {
    //         console.log('error');
    //     }
    // }
    // console.log('Dive iin');
    return (
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: 50, backgroundColor: '#7bc242', alignItems: 'center', paddingHorizontal: 15, position: 'absolute', width: '100%', zIndex: 999, bottom: 0 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={styles.bold}>{cartQuantity} </Text>
                <Text style={styles.bold}>Item</Text>
                <View style={{ marginHorizontal: 5 }}>
                    <Text style={styles.bold}>|</Text>
                </View>
                <Text style={styles.bold}> {'\u20B9'}</Text>
                <Text style={styles.bold}>{cartPrice}</Text>
            </View>
            <TouchableOpacity onPress={() => navigation.push('Cart')} style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text style={styles.bold}>View Cart</Text>
                <Image style={{ height: 25, width: 25 }}
                    source={require('../assets/img/cart_icon.png')}
                />
            </TouchableOpacity>
        </View>
    )
}
export default CartFooter

const styles = StyleSheet.create({
    bold: {
        fontFamily: 'Montserrat-Bold',
        color: '#fff'
    }
})
