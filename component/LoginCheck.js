export const LoginCheck = {
    _isLogedInCheck
};
import { useContext } from 'react';
import { LoginContext } from '../context/LoginContext';
function _isLogedInCheck() {
    const { userData, dispatch } = useContext(LoginContext);
    return userData && userData.length ? 1 : 0;
}

