import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Image, Text, TouchableOpacity, TouchableHighlight, Dimensions, StatusBar, FlatList, Button, Pressable } from 'react-native'
import { webService } from '../webService';
import Ripple from 'react-native-material-ripple';
import MenuBanner from './MenuBanner';
import MyText from './MyText';
import SubMenuPopup from './SubMenuPopup';
import CartFooter from './CartFooter';
import SubMenuFlatList from './SubMenuFlatList';
import { useNavigation } from '@react-navigation/native';
// import Style from '../assets/Style';
const menuImgUrl = 'http://www.foodpalava.com/assets/img/uploads/cuisine_img/';

const SubMenu = (props) => {
    const navigation = useNavigation();
    // console.log(props);
    const [menuList, setMenuList] = useState([]);
    const [subCuisineThmbnail, setSubCuisineThmbnail] = useState('')
    const { category_id, mainMenu } = props.route.params;
    const [menuShow, setMenuShow] = useState(false);
    // console.log(mainMenu);
    useEffect(() => {
        let apiEndpoint = 'Sub_menu_ctrl/get_cuisine';
        let header = webService.getHeaders({});
        let postArray = { category_id }
        //console.log(postArray)
        webService.post(apiEndpoint, postArray, header).then((response) => {
            if (response.status === 200) {
                response = response.data;
                if (response.success) {

                    // console.log(response);
                    const imageArray = response.data.map((item) => {
                        return item.image_name;
                    })
                    const randomNo = Math.floor(Math.random() * (imageArray.length - 0) + 0)
                    const randomImage = imageArray[randomNo];
                    setSubCuisineThmbnail(randomImage);
                    setMenuList(response.data);
                } else {
                    // Alert.alert('ERROR', response.message)
                }
            } else {
                // Alert.alert('ERROR', 'Net is not working')
            }
        });
        // clearData();

    }, []);
    return (
        <>
            {menuShow ? <SubMenuPopup mainMenu={mainMenu} setMenuShow={(value) => setMenuShow(value)} /> : null}
            <View style={{ backgroundColor: '#fff', flex: 1 }}>
                <View style={{ position: 'absolute', bottom: 60, zIndex: 999, width: '100%', alignItems: 'center' }}>
                    {!menuShow ? <Ripple onPressIn={() => setMenuShow(true)} style={{ backgroundColor: '#005b7f', paddingHorizontal: 20, paddingVertical: 10, borderRadius: 5 }}>
                        <View>
                            <MyText color="#fff">Menu</MyText>
                        </View>
                    </Ripple> : null}
                </View>
                <MenuBanner subCuisineThmbnail={subCuisineThmbnail} menuImgUrl={menuImgUrl} />
                <SubMenuFlatList menuList={menuList} hideCart={1} />
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                    {/* <Text>Go Back</Text> */}
                </TouchableOpacity>
            </View>
        </>
    )
}

export default SubMenu

