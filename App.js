import * as React from 'react';
import { Button, View } from 'react-native';
import { NavigationContainer, Image } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './component/Home';
import Menu from './component/Menu';
import SubMenu from './component/SubMenu';
import FoodSlider from './component/FoodSlider';
import styles from './assets/Style';
import Cart from './component/Cart';
import Login from './component/Login';
import { Provider as PaperProvider } from 'react-native-paper';
import Signup from './component/Signup';
import LoginContextProvider from './context/LoginContext';

const Stack = createStackNavigator();
function MyStack(props) {
  return (
    <LoginContextProvider>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen options={{
          title: ''
        }} name="Home" component={Home} />
        <Stack.Screen name="Menu" abc="razi" component={Menu} />
        <Stack.Screen name="SubMenu" component={SubMenu} />
        <Stack.Screen name="FoodSLider" component={FoodSlider} />
        <Stack.Screen name="Cart" component={Cart} />
        <Stack.Screen options={{ headerShown: false }} name="Login" component={Login} />
        <Stack.Screen options={{ headerShown: false }} name="Signup" component={Signup} />
      </Stack.Navigator>
    </LoginContextProvider>
  );
}
export default function App() {
  return (
    <NavigationContainer>
      <MyStack />
    </NavigationContainer>


  );
}